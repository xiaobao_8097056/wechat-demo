//index.js
//获取应用实例
const app = getApp()

Page({
  
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    bannerData:{
      imgUrls: [  
        'http://img02.tooopen.com/images/20150928/tooopen_sy_143912755726.jpg',  
        'http://img02.tooopen.com/images/20150928/tooopen_sy_143912755726.jpg',  
        'http://img02.tooopen.com/images/20150928/tooopen_sy_143912755726.jpg'  
      ],  
      indicatorDots: true,  
      autoplay: true,  
      interval: 3000,  
      duration: 1000
      
    },
    msgList: [
      { id: "1", title: "公告：多地首套房贷利率上浮 热点城市渐迎零折扣时代" },
      { id: "2", title: "公告：悦如公寓三周年生日趴邀你免费吃喝欢唱" },
      { id: "3", title: "公告：你想和一群有志青年一起过生日嘛？" }
    ],
    noticeNews:[
      {id:33, title:"沙县小吃",content:"你右手边沙县里面的炒粉丝还是可以的"},
      {id:36, title:"兰州拉面",content:"你左手边兰州拉面馆里面的东西还是很好吃的"}
    ],
    showRl:true,
    showFloat:true
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    wx.canIUse('button.open-type.getUserInfo');
     console.log(this.data);
    if (app.globalData.userInfo) {
      
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
        
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          console.log(res.userInfo);
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  loadData:function(){wx.setNavigationBarTitle({
      title: '首页'
    });
  },
  onReady: function () {  
    this.loadData();  
  },
  onShow: function () {

  },
  newsId: function (e) {  
    var that = this  
    var item = e.detail.current;  
    this.setData({  
      newsId:that.data.noticeNews[item].id  
    })  
  },  
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
  
})
